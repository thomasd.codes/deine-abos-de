<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Payment;
use App\Entity\Subscription;

class PaymentFactory
{
    public function createPaymentsForSubscription(Subscription $subscription): array
    {
        $returnBag = [];
        $creationDate = clone $subscription->getStartDate();

        for($i = 0; $i < 24; $i += $subscription->getPeriod() ) {
            $returnBag[] = (new Payment())
                ->setDate(clone $creationDate)
                ->setIsPaid(false)
                ->setAmount($subscription->getCosts());

            $creationDate->add(new \DateInterval(sprintf("P%dM", $subscription->getPeriod())));
        }

        return $returnBag;
    }
}
