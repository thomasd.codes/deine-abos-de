<?php

namespace App\Repository;

use App\Entity\Payment;
use App\Entity\Subscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Payment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Payment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Payment[]    findAll()
 * @method Payment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Payment::class);
    }

    public function getPaymentsForMonthAndYear(int $month, int $year): array
    {
        return $this
            ->createQueryBuilder('p')
            ->andWhere('YEAR(p.date) = :paymentYear')
            ->andWhere('MONTH(p.date) = :paymentMonth')
            ->setParameter('paymentYear', $year)
            ->setParameter('paymentMonth', $month)
            ->getQuery()
            ->execute();
    }
}
