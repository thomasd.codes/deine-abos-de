<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Payment;
use App\Entity\Subscription;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\{
    Bundle\FrameworkBundle\Controller\AbstractController,
    Component\HttpFoundation\Request,
    Component\HttpFoundation\Response,
    Component\Routing\Annotation\Route,
};

class PaymentController extends AbstractController
{
    public function __construct(private EntityManagerInterface $em)
    {
    }

    #[Route('/subscription/{id}/addPayment', name: 'add_payment', methods: ["POST"])]
    public function addPayment(Request $request, int $id): Response
    {
        $payment = $this->paymentFactory($request);
        /** @var Subscription $subscription */
        $subscription = $this->em->getRepository(Subscription::class)->find($id);
        $payment->setSubscription($subscription);
        $payment->setIsPaid(true);
        $this->em->persist($payment);
        $this->em->flush();

        return $this->render('payment/list.html.twig', [
            'payments' => $subscription->getPayments(),
        ]);
    }

    #[Route('/payment/{id}', name: 'api_payment_delete', methods: ["DELETE"])]
    public function deletePayment(int $id = 0): Response
    {
        /** @var Payment $payment */
        $payment = $this->em->getRepository(Payment::class)->find($id);
        $subscription = $payment->getSubscription();
        $this->em->remove($payment);
        $this->em->flush();

        return $this->render('payment/list.html.twig', [
            'payments' => $subscription->getPayments(),
        ]);
    }

    private function paymentFactory(Request $request): Payment
    {
        $formData = $request->request->get('payment');
        $date = new DateTime(
            sprintf("%s-%s-%s", $formData['date']['year'], $formData['date']['month'], $formData['date']['day'])
        );
        return (new Payment())
            ->setAmount((float)$formData['amount'])
            ->setDate($date);
    }
}
