<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\{
    Bundle\FrameworkBundle\Controller\AbstractController,
    Component\HttpFoundation\Response,
    Component\Routing\Annotation\Route,
};

class HomeController extends AbstractController
{
    #[Route('/', name: 'index',methods: ["GET"])]
    public function index(): Response
    {
        return $this->render('home/index.html.twig');
    }
}
