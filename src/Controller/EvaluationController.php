<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Payment;
use App\Repository\PaymentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\{
    Bundle\FrameworkBundle\Controller\AbstractController,
    Component\HttpFoundation\Response,
    Component\Routing\Annotation\Route,
};

class EvaluationController extends AbstractController
{
    public function __construct(private EntityManagerInterface $em)
    {
    }

    #[Route('/evaluation/{month}/{year}', name: 'app_evaluation', methods: ["GET"])]
    public function index(int $month = null, int $year = null): Response
    {
        $month = $month ?: (int)date('m');
        $year = $year ?: (int)date('Y');

        /** @var PaymentRepository $paymentRepo */
        $paymentRepo = $this->em->getRepository(Payment::class);

        return $this->render('evaluation/index.html.twig', [
            'month' => $month,
            'year' => $year,
            'paid' => $paymentRepo->getPaymentsForMonthAndYear($month, $year),
            'unpaid' => [],
        ]);
    }
}
