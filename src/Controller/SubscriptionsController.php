<?php

declare(strict_types=1);

namespace App\Controller;

use App\{
    Entity\Subscription,
    Form\SubscriptionType,
};
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\{
    HttpFoundation\Request,
    HttpFoundation\Response,
    Routing\Annotation\Route,
};

#[Route("/app")]
class SubscriptionsController extends AbstractController
{
    #[Route('/subscriptions', name: 'app_subscriptions_list', methods: ["GET"])]
    public function list(): Response
    {
        $subscriptions = $this->getDoctrine()->getRepository(Subscription::class)
            ->findBy(['user' => $this->getUser()]);

        return $this->render('subscription/list.html.twig', [
            'subscriptions' => $subscriptions
        ]);
    }

    #[Route('/subscription/new', name: 'newSubscription', methods: ["GET", "POST"])]
    public function new(Request $request): Response
    {
        $subscription = new Subscription();
        $subscription->setStartDate(new \DateTime());

        $form = $this->createForm(SubscriptionType::class, $subscription);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $subscription->setUser($this->getUser());
            //$payments = $this->paymentFactory->createPaymentsForSubscription($subscription);
            //$subscription->setPayments($payments);
            $em->persist($subscription);
            $em->flush();
            return $this->redirectToRoute('app_subscriptions_list');
        }

        return $this->render('subscription/new.html.twig', [
            'form' => $form->createView()
        ]);
    }


    #[Route('/subscription/{id<\d+>}', name: 'detailSubscription', methods: ["GET"])]
    public function detail(int $id, Request $request): Response
    {
        $subscription = $this->getDoctrine()->getRepository(Subscription::class)->find($id);
        $this->denyAccessUnlessGranted('MANAGE', $subscription);

        $subscriptionForm = $this->createForm(SubscriptionType::class, $subscription);
        $paymentForm = $this->createForm(\App\Form\PaymentType::class);

        $subscriptionForm->handleRequest($request);
        if ($subscriptionForm->isSubmitted() && $subscriptionForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
        }

        return $this->render('subscription/detail.html.twig', [
            'subscription' => $subscription,
            'subscriptionForm' => $subscriptionForm->createView(),
            'paymentForm' => $paymentForm->createView(),
        ]);
    }
}
