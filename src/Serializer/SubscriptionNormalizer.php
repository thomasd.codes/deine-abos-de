<?php

declare(strict_types=1);

namespace App\Serializer;

use App\Entity\Subscription;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class SubscriptionNormalizer implements ContextAwareNormalizerInterface
{

    protected RouterInterface $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param Subscription $object
     * @param string|null $format
     * @param array $context
     * @return array|\ArrayObject|bool|float|int|string|null
     */
    public function normalize($object, string $format = null, array $context = []): array
    {
        $returnData = [];
        $returnData['type'] = 'subscription';
        $returnData['id'] = $object->getId();
        $returnData['attributes'] = [
            'name' => $object->getName(),
            'startDate' => $object->getStartDate()
        ];
        $returnData['links'] = [
            'self' => $this->router->generate('readSubscription', ['id' => $object->getId()])
        ];

        $this->createRelationLinks($object, $returnData);

        return $returnData;
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof Subscription;
    }

    protected function createRelationLinks(Subscription $subscription, array &$returnData): void
    {
        if ($subscription->getPaymentType()) {
            $returnData['relationships'] = [
                'paymentType' => [
                    'links' => [
                        'related' => $this->router->generate('readPaymentType', ['id' => $subscription->getPaymentType()->getId()])
                    ]
                ]
            ];
        }
    }
}
