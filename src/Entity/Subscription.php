<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Traits\EntityIdTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubscriptionRepository")
 */
class Subscription
{
    use EntityIdTrait;

    public const YEARLY_SUBSCRIPTION = 12;
    public const HALF_YEARLY_SUBSCRIPTION = 6;
    public const QUARTER_YEARLY_SUBSCRIPTION = 3;
    public const MONTHLY_SUBSCRIPTION = 1;


    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    protected string $name;

    /**
     * @ORM\Column(type="date")
     */
    protected DateTime $startDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PaymentType", inversedBy="subscriptions")
     */
    protected ?PaymentType $paymentType;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected ?float $costs = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected ?int $period = null;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class)
     */
    private ?Category $category;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="subscriptions")
     */
    private User $user;

    /**
     * @ORM\OneToMany(targetEntity=Payment::class, mappedBy="subscription", orphanRemoval=true)
     */
    private Selectable $payments;

    public function __construct()
    {
        $this->payments = new ArrayCollection();
    }

    public function addAdditionalInformation(array $info): self
    {
        $additionalInformation[] = $info;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getStartDate(): DateTime
    {
        return $this->startDate;
    }

    public function setStartDate(DateTime $startDate): self
    {
        $this->startDate = $startDate;
        return $this;
    }

    public function getPaymentType(): ?PaymentType
    {
        return $this->paymentType;
    }

    public function setPaymentType(?PaymentType $paymentType): self
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    public function getCosts(): ?float
    {
        return $this->costs;
    }

    public function setCosts(float $costs): self
    {
        $this->costs = $costs;
        return $this;
    }

    public function getPeriod(): ?int
    {
        return $this->period;
    }

    public function setPeriod(?int $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setSubscription($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->contains($payment)) {
            $this->payments->removeElement($payment);
            // set the owning side to null (unless already changed)
            if ($payment->getSubscription() === $this) {
                $payment->setSubscription(null);
            }
        }

        return $this;
    }
}
