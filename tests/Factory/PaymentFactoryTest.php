<?php

namespace App\Tests\Factory;

use App\Entity\Payment;
use App\Entity\Subscription;
use App\Factory\PaymentFactory;
use PHPUnit\Framework\TestCase;

class PaymentFactoryTest extends TestCase
{
    private float $costs;
    private Subscription $subscription;
    private PaymentFactory $paymentFactory;
    private \DateTime $startDate;

    protected function setUp()
    {
        $this->costs = 7.99;
        $this->startDate = new \DateTime();
        $this->subscription = (new Subscription())
            ->setPeriod(Subscription::YEARLY_SUBSCRIPTION)
            ->setStartDate($this->startDate)
            ->setCosts($this->costs);
        $this->paymentFactory = new PaymentFactory();

        parent::setUp();
    }

    public function testCreatePaymentsForSubscriptionReturnsArray()
    {
        //Ausführungsphase
        $result = $this->paymentFactory->createPaymentsForSubscription($this->subscription);

        //Testphase
        $this->assertIsArray($result);
    }

    public function testCreatePaymentsForSubscriptionReturnsArrayWithFourEntries()
    {
        $this->subscription->setPeriod(Subscription::HALF_YEARLY_SUBSCRIPTION);

        //Ausführungsphase
        $result = $this->paymentFactory->createPaymentsForSubscription($this->subscription);

        //Testphase
        $this->assertCount(4, $result);
    }

    public function testCreatePaymentsForSubscriptionReturnsArrayWithTwoEntries()
    {
        //Ausführungsphase
        $result = $this->paymentFactory->createPaymentsForSubscription($this->subscription);

        //Testphase
        $this->assertCount(2, $result);
    }

    //Test for monthly subscription
    public function testCreatePaymentsForSubscriptionReturnsArrayWithTwentyFourEntries()
    {
        //Vorbereitungsphase
        $this->subscription->setPeriod(Subscription::MONTHLY_SUBSCRIPTION);

        //Ausführungsphase
        $result = $this->paymentFactory->createPaymentsForSubscription($this->subscription);

        print_r($result);

        //Testphase
        $this->assertCount(24, $result);
    }

    public function testCreatePaymentsForSubscriptionReturnsPaymentsWithIsPaidFalse()
    {
        $result = $this->paymentFactory->createPaymentsForSubscription($this->subscription);

        /** @var Payment $payment */
        foreach ($result as $payment) {
            $this->assertFalse($payment->getIsPaid());
        }
    }

    public function testCreatePaymentsForSubscriptionReturnsPaymentsWithSameAmountAsSubscription()
    {
        $result = $this->paymentFactory->createPaymentsForSubscription($this->subscription);

        /** @var Payment $payment */
        foreach ($result as $payment) {
            $this->assertEquals($this->costs, $payment->getAmount());
        }
    }

    public function testCreatePaymentsForSubscriptionReturnsPaymentsWithValidDate()
    {
        $testDate = clone $this->startDate;
        $result = $this->paymentFactory->createPaymentsForSubscription($this->subscription);

        /** @var Payment $payment */
        foreach ($result as $payment) {
            $this->assertEquals($testDate, $payment->getDate());
            $testDate->add(new \DateInterval(sprintf("P%dM", $this->subscription->getPeriod())));
        }
    }
}
